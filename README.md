
# SYNTHESE JQUERY

## Approche pour me simplifier la vie en JQuery
***

***
### Un peu de blabla
***

**Fonction jQuery**

C'est le point d'entrée de la bibliothèque jQuery. Vous pouvez utiliser au choix l'instruction jQuery() ou son alias $().

**Méthodes jQuery**

La bibliothèque jQuery est constituée d'un ensemble de blocs de code autonomes appelés **méthodes**. Ce qui fait la puissance de cette bibliothèque, c'est avant tout la grande diversité des méthodes proposées. Pour exécuter une *méthode* jQuery, il suffit de préciser son nom à la suite d'un *sélecteur* en le séparant de ce dernier par un *point* : **$(sélecteur).méthode(paramètres);.**

**Objet jQuery**

On appelle « objet jQuery » l'entité retournée par la fonction jQuery, c'est-à-dire par $(). Cet objet représente un ensemble de zéro, un ou plusieurs éléments issus du document.
